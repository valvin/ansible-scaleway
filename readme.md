# Valvin Ansible playbooks for my servers

Initially this repository contained my infrastructure as code using ansible for scaleway.

After a migration from Scaleway to Hetzner I decided to not use Ansible as a tool to manage my infrastructure.

I would recommend using Terraform for this but to be honest I have not terraformed my Hetzner infrastructure.


This repository contains playbooks and inventory of my hetzner servers.


## Inventory

I'm using `hcloud` plugin inventory to get servers information and tags.

Servers are allocated in groups using their tags.

Secrets are stored in a vault file and is the same for all servers.

Variables could be hosted on different levels:

* variables for all servers
* variables for a specific group
* variables for a specific host


## Playbooks

### MySQL

Using mysql modules we are creating users and grant them priviledges to their database and create this database empty or restoring it.

### PostgreSQL

Using postesql modules we are creating users, empty databases or restoring it using dumps.

### Servers

now only packages installation and upgrade
