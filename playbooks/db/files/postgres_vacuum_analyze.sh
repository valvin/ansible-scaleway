#!/bin/bash
# vacuum_analyze.sh
SCRIPT=`readlink -f $0`
SCRIPTPATH=`dirname $SCRIPT`
echo "START TIME - "$(date)
for bdd in `psql -At -c "select datname from pg_database where datistemplate=false and datname!='postgres' and pg_is_in_recovery()=false;" `
do
 printf "%s : " $bdd
 psql -d $bdd -c "vacuum;"
 psql -d $bdd -c "analyze;"
done
echo "END TIME   - "$(date)
